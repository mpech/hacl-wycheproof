#include <stdlib.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>
#include "monocypher.h"
#include "monocypher-ed25519.h"
#include "Hacl_Ed25519.h"
#include "Hacl_Hash.h"

#define ARRAY(name, size) \
    uint8_t name[size]; \
    for(size_t i = 0; i < size; i++) name[i] = i;

int sign_check_ed25519(void) {
    ARRAY(hash1, 64);
    ARRAY(hash2, 64);
    ARRAY(key,   32);
    ARRAY(pub1,  32);
    ARRAY(pub2,  32);
    ARRAY(in,    32);
    int status = 0;
    
    crypto_ed25519_public_key(pub1, key);
	Hacl_Ed25519_secret_to_public(pub2, key);
	status |= crypto_verify32(pub1, pub2);
    
    crypto_ed25519_sign(hash1, key, pub1, in, 32);
    Hacl_Ed25519_sign(hash2, key, 32, in);
	status |= crypto_verify64(hash1, hash2);
    
    status |= crypto_ed25519_check(hash1, pub1, in, 32);
    status |= !Hacl_Ed25519_verify(pub2, 32, in, hash2);	// as bool: 1 - ok, 0 - wrong

	// XXX
    for(size_t i = 0; i < 64; i++) hash2[i] = 0;
    // must fail
    status |= Hacl_Ed25519_verify(hash2, 0, hash2, hash2);		// as bool: 1 - ok, 0 - wrong
    
    return status;
}

int main(void) {
	int status = 0;
	
	status |= sign_check_ed25519();

	printf("%s\n", status != 0 ? "FAIL" : "OK");	
	return status;
}
